package com.example.musicplayer.network.json.artist

data class Section(
    val type: String,
    val hits: List<Hit>
)