package com.example.musicplayer.network

import com.example.musicplayer.network.json.album.Album
import com.example.musicplayer.network.json.artist.Artist
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GeniusApi {

    @GET("api/search/artist")
    fun artists(@Query("q") name: String, @Query("page") page: Int = 1) : Call<Artist>

    @GET("api/search/album")
    fun albums(@Query("q") title: String, @Query("page") page: Int = 1) : Call<Album>

}