package com.example.musicplayer.network.json.album

data class Result(
    val name: String,
    val cover_art_thumbnail_url: String
)