package com.example.musicplayer.network.json.album

data class Response(
    val sections : List<Section>
)