package com.example.musicplayer.network.json.artist

data class Response(
    val sections : List<Section>
)