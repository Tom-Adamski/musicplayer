package com.example.musicplayer.network.json.artist

data class Result(
    val name: String,
    val image_url: String
)