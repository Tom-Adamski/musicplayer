package com.example.musicplayer.network.json.album

data class Section(
    val type: String,
    val hits: List<Hit>
)