package com.example.musicplayer.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Song(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "albumId")
    val albumId: Long,
    @ColumnInfo(name = "artistId")
    val artistId: Long,
    @ColumnInfo(name = "track")
    val track: Int,
    @ColumnInfo(name = "duration")
    val duration: Int
)