package com.example.musicplayer.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.musicplayer.database.entity.AlbumDetails
import com.example.musicplayer.database.entity.ArtistDetails
import com.example.musicplayer.database.entity.SongDetails

@Dao
interface DetailsDao {

    @Query("SELECT $SONG_DETAILS FROM Song, Artist, Album $WHERE_SONG_ALL ORDER BY UPPER(Song.title)")
    fun getAllSongs() : List<SongDetails>

    @Query("SELECT $SONG_DETAILS FROM Song, Artist, Album $WHERE_SONG_FOR_ARTIST ORDER BY Album.title, Song.track")
    fun getAllSongsForArtist(artistId : Long) : List<SongDetails>

    @Query("SELECT $SONG_DETAILS FROM Song, Artist, Album $WHERE_SONG_FOR_ALBUM ORDER BY Song.track")
    fun getAllSongsForAlbum(albumId : Long) : List<SongDetails>

    @Query("SELECT a.id as id, a.name as name, a.thumbnail as thumbnail,(SELECT COUNT(*) FROM Song s WHERE a.id = s.artistId) as own,(SELECT COUNT(*) FROM Featuring f WHERE a.id = f.artistId) as feats,(SELECT COUNT(*)FROM Album al WHERE a.id = al.artistId) as albums FROM Artist a ORDER BY a.name")
    fun getAllArtists() : List<ArtistDetails>

    @Query("SELECT a.id as id, a.title as title, a.thumbnail as thumbnail, a.date as date, (SELECT COUNT(*) FROM Song s WHERE a.id = s.albumId) as tracks, (SELECT ar.name FROM Artist ar WHERE a.artistId = ar.id) as artistName FROM Album a ORDER BY a.title")
    fun getAllAlbums() : List<AlbumDetails>

    companion object {
        const val SONG_DETAILS = "Song.id as id, Song.title as title, Song.track as track, Song.duration as duration, Artist.name as artistName, Album.title as albumTitle, Album.thumbnail as thumbnail"
        const val WHERE_SONG_ALL = "WHERE Song.albumId = Album.id AND Song.artistId = Artist.id"
        const val WHERE_SONG_FOR_ARTIST = "WHERE (Song.id IN (SELECT Featuring.songId FROM Featuring WHERE Featuring.artistId = :artistId) OR Song.artistId = :artistId) AND Song.artistId = Artist.id AND Song.albumId = Album.id"
        const val WHERE_SONG_FOR_ALBUM = "WHERE Song.albumId = Album.id AND Song.artistId = Artist.id AND Album.id = :albumId "

    }

}