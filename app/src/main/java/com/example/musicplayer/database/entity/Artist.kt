package com.example.musicplayer.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Artist(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "name", index = true)
    val name: String,
    @ColumnInfo(name = "thumbnail")
    val thumbnail: String?
) {
    constructor(name: String, thumbnail: String?) : this(0, name, thumbnail)
}