package com.example.musicplayer.database.entity

data class SongDetails(
    val id: Long,
    val title: String,
    val track: Int,
    val duration: Int,
    val artistName: String,
    val albumTitle: String,
    val thumbnail: String?
)