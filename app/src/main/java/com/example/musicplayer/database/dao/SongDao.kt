package com.example.musicplayer.database.dao

import androidx.room.*
import com.example.musicplayer.database.entity.Song


@Dao
interface SongDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(song: Song) : Long

    @Delete
    fun delete(song: Song)
}