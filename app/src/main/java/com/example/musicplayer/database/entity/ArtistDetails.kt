package com.example.musicplayer.database.entity

data class ArtistDetails(
    val id: Long,
    val name: String,
    val thumbnail: String?,
    val own : Int,
    val feats : Int,
    val albums : Int
)