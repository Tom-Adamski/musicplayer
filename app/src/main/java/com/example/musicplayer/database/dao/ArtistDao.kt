package com.example.musicplayer.database.dao

import androidx.room.*
import com.example.musicplayer.database.entity.Artist

@Dao
interface ArtistDao {
    @Query("SELECT * FROM artist ORDER BY UPPER(name) ASC")
    fun getAll() : List<Artist>

    @Query("SELECT * FROM artist WHERE thumbnail IS NULL ORDER BY UPPER(name) ASC")
    fun getAllWithoutThumbnail() : List<Artist>

    @Query("SELECT * FROM artist WHERE id = :artistId LIMIT 1")
    fun loadById(artistId: Long): Artist

    @Query("SELECT * FROM artist WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String) : Artist?

    @Query("SELECT Count(*) FROM Artist")
    fun getCount() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(artist: Artist) : Long

    @Delete
    fun delete(artist: Artist)

    @Transaction
    fun upsert(name: String, thumbnail : String?) : Long {
        var id : Long
        val artist = findByName(name)
        id = if(artist == null){
            insert(Artist(name, thumbnail))
        } else {
            artist.id
        }
        return id
    }

    @Query("UPDATE artist SET thumbnail = :thumbnail WHERE id = :id")
    fun updateThumbnail(id: Long, thumbnail: String)

}