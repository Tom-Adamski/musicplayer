package com.example.musicplayer.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Featuring(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "songId")
    val songId: Long,
    @ColumnInfo(name = "artistId")
    val artistId: Long
) {
    constructor(songId: Long, artistId: Long) : this(0, songId, artistId)
}