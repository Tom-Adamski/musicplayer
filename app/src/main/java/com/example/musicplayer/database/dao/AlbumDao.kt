package com.example.musicplayer.database.dao

import androidx.room.*
import com.example.musicplayer.database.entity.Album

@Dao
interface AlbumDao {
    @Query("SELECT * FROM Album ORDER BY UPPER(title) ASC")
    fun getAll() : List<Album>

    @Query("SELECT * FROM album WHERE thumbnail IS NULL ORDER BY UPPER(title) ASC")
    fun getAllWithoutThumbnail() : List<Album>

    @Query("SELECT * FROM Album WHERE artistId = :artistId ORDER BY UPPER(title) ASC")
    fun getAllFromArtist(artistId: Long) : List<Album>

    @Query("SELECT * FROM Album WHERE id = :albumId LIMIT 1")
    fun loadById(albumId: Long): Album

    @Query("SELECT * FROM Album WHERE title LIKE :title")
    fun searchByTitle(title: String) : List<Album>

    @Query("SELECT * FROM Album WHERE title LIKE :title AND artistId = :artistId LIMIT 1")
    fun findByDetails(title: String, artistId: Long) : Album?

    @Query("SELECT Count(*) FROM Album")
    fun getCount() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(album: Album) : Long

    @Delete
    fun delete(album: Album)

    @Transaction
    fun upsert(title: String, artistId: Long, thumbnail : String?, date: String?) : Long {
        var id : Long
        val album = findByDetails(title, artistId)
        if(album == null){
            id = insert(Album(title, artistId, thumbnail, date))
        }
        else {
            id = album.id
        }
        return id
    }

    @Query("UPDATE album SET thumbnail = :thumbnail WHERE id = :id")
    fun updateThumbnail(id: Long, thumbnail: String)
}