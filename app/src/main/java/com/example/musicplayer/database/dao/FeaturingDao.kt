package com.example.musicplayer.database.dao

import androidx.room.*
import com.example.musicplayer.database.entity.Featuring

@Dao
interface FeaturingDao {
    @Query("SELECT * FROM Featuring")
    fun getAll() : List<Featuring>

    @Query("SELECT * FROM Featuring WHERE songId = :songId AND artistId = :artistId LIMIT 1")
    fun findFromIds(songId: Long, artistId: Long): Featuring?

    @Query("SELECT * FROM Featuring WHERE songId = :songId")
    fun getFeaturingsForSong(songId: Long): List<Featuring>

    @Query("SELECT * FROM Featuring WHERE artistId = :artistId")
    fun getFeaturingsForArtist(artistId: Long): List<Featuring>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(featuring: Featuring) : Long

    @Delete
    fun delete(featuring: Featuring)

    @Transaction
    fun upsert(songId: Long, artistId: Long) : Long {
        var id : Long
        val featuring = findFromIds(songId, artistId)
        if(featuring == null){
            id = insert(Featuring(songId, artistId))
        }
        else {
            id = featuring.id
        }
        return id
    }
}