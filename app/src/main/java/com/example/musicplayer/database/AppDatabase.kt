package com.example.musicplayer.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.musicplayer.database.dao.*
import com.example.musicplayer.database.entity.Album
import com.example.musicplayer.database.entity.Artist
import com.example.musicplayer.database.entity.Featuring
import com.example.musicplayer.database.entity.Song

@Database(entities = [
    Artist::class,
    Album::class,
    Song::class,
    Featuring::class
],
    version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun artistDao(): ArtistDao
    abstract fun albumDao(): AlbumDao
    abstract fun songDao(): SongDao
    abstract fun featuringDao(): FeaturingDao
    abstract fun detailsDao(): DetailsDao


    fun resetDatabase(){
        this.clearAllTables()
    }


}