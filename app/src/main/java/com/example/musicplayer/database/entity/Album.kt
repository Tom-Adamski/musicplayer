package com.example.musicplayer.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Album(
    @PrimaryKey(autoGenerate = true)
    val id : Long,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "artistId")
    val artistId: Long,
    @ColumnInfo(name = "thumbnail")
    val thumbnail: String?,
    @ColumnInfo(name = "date")
    val date: String?
) {
    constructor(title: String, artistId: Long, thumbnail: String?, date: String?) : this(0, title, artistId, thumbnail, date)
}