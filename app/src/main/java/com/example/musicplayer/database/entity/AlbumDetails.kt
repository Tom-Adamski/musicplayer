package com.example.musicplayer.database.entity

data class AlbumDetails(
    val id : Long,
    val title: String,
    val thumbnail: String?,
    val date: String?,
    val artistName: String,
    val tracks: Int
)