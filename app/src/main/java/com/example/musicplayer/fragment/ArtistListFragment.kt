package com.example.musicplayer.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.musicplayer.R
import com.example.musicplayer.adapter.ArtistDetailsListAdapter
import com.example.musicplayer.database.AppDatabase
import com.example.musicplayer.database.dao.DetailsDao
import com.example.musicplayer.database.entity.ArtistDetails
import com.example.musicplayer.utils.FragmentListener
import com.example.musicplayer.utils.OnListClickListener
import kotlinx.android.synthetic.main.fragment_artist_list.view.*

class ArtistListFragment(private val fragmentListener: FragmentListener) : Fragment(), OnListClickListener {

    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    private lateinit var db: AppDatabase
    private lateinit var detailsDao: DetailsDao

    private val artistList = ArrayList<ArtistDetails>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflated = inflater.inflate(R.layout.fragment_artist_list, container, false)

        viewManager = LinearLayoutManager(context)

        db = Room.databaseBuilder(context!!, AppDatabase::class.java, "music-database").build()
        detailsDao = db.detailsDao()

        inflated.fastscroll.setRecyclerView(inflated.recycler_view)

        AsyncTask.execute {
            detailsDao.getAllArtists().forEach {
                artistList.add(it)
            }

            viewAdapter = ArtistDetailsListAdapter(artistList, this)

            activity?.runOnUiThread {
                inflated.recycler_view.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
                inflated.fastscroll.setRecyclerView(inflated.recycler_view)
            }

        }

        return inflated
    }

    override fun onListClick(position: Int) {

        fragmentListener.onArtistClick(artistList[position].id)

    }


}