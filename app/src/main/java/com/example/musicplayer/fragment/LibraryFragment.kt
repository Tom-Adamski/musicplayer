package com.example.musicplayer.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.musicplayer.R
import com.example.musicplayer.ui.LibrarySectionsPagerAdapter
import kotlinx.android.synthetic.main.fragment_library.view.*

class LibraryFragment : Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflated = inflater.inflate(R.layout.fragment_library, container, false)

        val sectionsPagerAdapter = LibrarySectionsPagerAdapter(context!!, activity!!.supportFragmentManager)

        inflated.view_pager.adapter = sectionsPagerAdapter
        inflated.view_pager.offscreenPageLimit = LibrarySectionsPagerAdapter.TABS_COUNT
        inflated.tabs.setupWithViewPager(inflated.view_pager)


        return inflated
    }








}