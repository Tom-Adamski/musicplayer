package com.example.musicplayer.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.musicplayer.activity.MainActivity
import com.example.musicplayer.database.entity.SongDetails
import com.example.musicplayer.service.PlaybackService
import com.example.musicplayer.utils.OnListClickListener
import com.example.musicplayer.utils.SongQueue

open class SongStarterFragment : Fragment() , OnListClickListener {

    protected val songDetailsList = ArrayList<SongDetails>()

    lateinit var songQueue: SongQueue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        songQueue = SongQueue.getInstance(context!!)
    }

    override fun onListClick(position: Int) {
        songQueue.setQueue(songDetailsList)
        songQueue.pos = position
        songQueue.hasChanged = true

        val intent = Intent(context, PlaybackService::class.java)
        intent.action = PlaybackService.ACTION_START_QUEUE
        activity?.startService(intent)

        (activity as MainActivity).updateController()
    }








}