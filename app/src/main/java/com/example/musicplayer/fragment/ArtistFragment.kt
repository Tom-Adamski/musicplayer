package com.example.musicplayer.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.musicplayer.R
import com.example.musicplayer.adapter.SongDetailsListAdapter
import com.example.musicplayer.database.AppDatabase
import com.example.musicplayer.database.dao.*
import kotlinx.android.synthetic.main.fragment_artist.view.*

class ArtistFragment(private val artistId : Long) : SongStarterFragment(){

    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    private lateinit var db: AppDatabase
    private lateinit var detailsDao: DetailsDao


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflated = inflater.inflate(R.layout.fragment_artist, container, false)

        viewManager = LinearLayoutManager(context)


        db = Room.databaseBuilder(context!!, AppDatabase::class.java, "music-database").build()
        detailsDao = db.detailsDao()


        AsyncTask.execute {

            detailsDao.getAllSongsForArtist(artistId).forEach {
                songDetailsList.add(it)
            }

            viewAdapter = SongDetailsListAdapter(songDetailsList, this)

            activity?.runOnUiThread {
                inflated.recycler_view.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
            }

        }

        return inflated
    }

}