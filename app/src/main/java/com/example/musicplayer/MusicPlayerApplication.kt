package com.example.musicplayer

import android.app.Application
import com.facebook.stetho.Stetho

class MusicPlayerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

}