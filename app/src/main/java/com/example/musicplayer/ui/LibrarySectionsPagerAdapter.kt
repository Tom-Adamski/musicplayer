package com.example.musicplayer.ui

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.musicplayer.activity.MainActivity
import com.example.musicplayer.fragment.AlbumListFragment
import com.example.musicplayer.fragment.ArtistListFragment
import com.example.musicplayer.fragment.SongListFragment


class LibrarySectionsPagerAdapter (private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    /* if this class is created from a different activity it will crash on cast */
    private val songListFragment = SongListFragment()
    private val artistListFragment = ArtistListFragment(context as MainActivity)
    private val albumListFragment = AlbumListFragment(context as MainActivity)


    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> songListFragment
            1 -> artistListFragment
            2 -> albumListFragment
            else -> Fragment()
        }
    }

    override fun getCount(): Int {
        return TABS_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }


    companion object {
        val TAB_TITLES = arrayOf(
            "Songs", "Artists", "Albums"
        )

        const val TABS_COUNT = 3

    }


}