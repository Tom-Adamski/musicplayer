package com.example.musicplayer.service

import android.app.PendingIntent
import android.content.*
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.PowerManager
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import androidx.media.MediaBrowserServiceCompat
import androidx.media.session.MediaButtonReceiver
import com.example.musicplayer.R
import com.example.musicplayer.utils.MusicNotificationManager
import com.example.musicplayer.utils.SongQueue
import java.io.IOException


class PlaybackService : MediaBrowserServiceCompat(),
    MediaPlayer.OnCompletionListener,
    MediaPlayer.OnErrorListener,
    MediaPlayer.OnPreparedListener,
    AudioManager.OnAudioFocusChangeListener {


    private var gesMediaPlayer: MediaPlayer? = null
    private var gesMediaSession: MediaSessionCompat? = null
    private var mTransportControls: MediaControllerCompat.TransportControls? = null

    private var activeAudio: Uri? = null
    private var pausedPosition = -1

    lateinit var songQueue: SongQueue

    private lateinit var musicNotificationManager: MusicNotificationManager


    //----------------------------------------Initializers----------------------------------------//

    private fun initMediaPlayer() {
        gesMediaPlayer = MediaPlayer()
        gesMediaPlayer!!.setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        gesMediaPlayer!!.setAudioAttributes(
            AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )
        gesMediaPlayer!!.setVolume(1.0f, 1.0f)
        gesMediaPlayer!!.setOnErrorListener(this)
        gesMediaPlayer!!.setOnPreparedListener(this)
        gesMediaPlayer!!.setOnCompletionListener(this)
    }


    private fun initMediaSession() {
        val mediaButtonReceiver = ComponentName(applicationContext, MediaButtonReceiver::class.java)
        gesMediaSession =
            MediaSessionCompat(applicationContext, "MediaService", mediaButtonReceiver, null)

        gesMediaSession!!.setCallback(mediaSessionCallbacks)
        gesMediaSession!!.setFlags(
            MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS
                    or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS
        )
        mTransportControls = gesMediaSession!!.controller.transportControls


        //pre lollipop media button handling
        val mediaButtonIntent = Intent(Intent.ACTION_MEDIA_BUTTON)
        mediaButtonIntent.setClass(this, MediaButtonReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0)
        gesMediaSession!!.setMediaButtonReceiver(pendingIntent)

        sessionToken = gesMediaSession!!.sessionToken
    }


    private fun initSessionMetadata() {
        val metadataBuilder = MediaMetadataCompat.Builder()
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, "Unknown song");
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "Unknown artist");
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "Unknown album");

        gesMediaSession?.setMetadata(metadataBuilder.build())
    }

    // Headphones unplugged
    private fun registerNoisyReceiver() {
        //TODO AudioManagerCompat ??
        val noisyFilter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
        registerReceiver(becomingNoisyReceiver, noisyFilter)
    }

    private fun registerAudioLoadReceiver() {
        /* TODO
        val filter = IntentFilter(SongsFragment.BROADCAST_AUDIO_LOAD_COMPLETE)
        registerReceiver(audioLoadComplete, filter)
        */

    }

    private fun registerAudioLoadAsyncReceiver() {
        /* TODO
        val filter = IntentFilter(SongsFragment.BROADCAST_AUDIO_LOAD_ASYNC_COMPLETE)
        registerReceiver(audioLoadAsyncComplete, filter)

         */
    }

    private fun registerNotificationCommandReceiver() {
        val notificationFilter = IntentFilter()
        notificationFilter.addAction(ACTION_PLAY)
        notificationFilter.addAction(ACTION_PAUSE)
        notificationFilter.addAction(ACTION_NEXT)
        notificationFilter.addAction(ACTION_PREVIOUS)
        registerReceiver(notificationPlaybackCommand, notificationFilter)
    }

    private fun setMediaPlaybackState(state: Int) {
        val playbackstateBuilder = PlaybackStateCompat.Builder()
        if (state == PlaybackStateCompat.STATE_PLAYING) {
            playbackstateBuilder.setActions(
                PlaybackStateCompat.ACTION_PLAY_PAUSE or PlaybackStateCompat.ACTION_PAUSE
                        or PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or PlaybackStateCompat.ACTION_SKIP_TO_NEXT
            )
        } else {
            playbackstateBuilder.setActions(
                PlaybackStateCompat.ACTION_PLAY_PAUSE or PlaybackStateCompat.ACTION_PLAY
                        or PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or PlaybackStateCompat.ACTION_SKIP_TO_NEXT
            )
        }
        playbackstateBuilder.setState(state, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0.0f)
        gesMediaSession?.setPlaybackState(playbackstateBuilder.build())
    }

    private fun updateMetadata() {
        gesMediaSession?.setMetadata(MediaMetadataCompat.Builder()
            //TODO .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, **bitmap**)
            .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, songQueue.queue[songQueue.pos].artistName)
            .putString(MediaMetadataCompat.METADATA_KEY_TITLE, songQueue.queue[songQueue.pos].title)
            .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, songQueue.queue[songQueue.pos].albumTitle)
            .build()
        )
    }

    //----------------------------------------Lifecycle Methods----------------------------------------//

    override fun onCreate() {
        super.onCreate()

        registerAudioLoadReceiver()
        registerAudioLoadAsyncReceiver()
        initMediaPlayer()
        initMediaSession()
        initSessionMetadata()
        callStateListener()

        musicNotificationManager = MusicNotificationManager.getInstance(applicationContext)

        songQueue = SongQueue.getInstance(applicationContext)


        val playbackStateBuilder = PlaybackStateCompat.Builder()
            .setActions(
                PlaybackStateCompat.ACTION_PLAY or
                        PlaybackStateCompat.ACTION_PLAY_PAUSE
            )
        gesMediaSession?.setPlaybackState(playbackStateBuilder.build())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        isServiceStarted = true
        registerNotificationCommandReceiver()

        if (intent?.action == ACTION_START_QUEUE) {
            refreshQueueAndStart()
        }
        else {
            MediaButtonReceiver.handleIntent(gesMediaSession, intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE)
        }
        gesMediaPlayer?.release()
        gesMediaSession?.release()
        unregisterReceiver(audioLoadAsyncComplete)
        unregisterReceiver(audioLoadComplete)
    }

    //----------------------------------------Media Player Listeners----------------------------------------//

    override fun onPrepared(p0: MediaPlayer?) {
        gesMediaSession?.isActive = true
        updateMetadata()
        gesMediaPlayer?.start()
        setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING)
        val state = gesMediaSession?.controller?.playbackState
        val token = gesMediaSession!!.sessionToken
        musicNotificationManager.showNotification(state!!, token)
        registerNoisyReceiver()
    }

    override fun onCompletion(mediaPlayer: MediaPlayer?) {
        if (gesMediaPlayer != null) {
            mTransportControls?.skipToNext()

        }
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        when (what) {
            MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK $extra"
            )
            MediaPlayer.MEDIA_ERROR_SERVER_DIED -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR SERVER DIED $extra"
            )
            MediaPlayer.MEDIA_ERROR_UNKNOWN -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR UNKNOWN $extra"
            )
            MediaPlayer.MEDIA_ERROR_UNSUPPORTED -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR UNSUPPORTED $extra"
            )
        }
        return false
    }

    //----------------------------------------Media Playback Functions----------------------------------------//

    private val mediaSessionCallbacks: MediaSessionCompat.Callback =
        object : MediaSessionCompat.Callback() {
            override fun onPlay() {
                super.onPlay()
                if (!requestAudioFocus()) {
                    return
                }

                if (!isServiceStarted) {
                    startService(Intent(applicationContext, PlaybackService::class.java))
                }

                if(lastAudioIndex != songQueue.pos) {
                    setSourceFromQueue()
                }

                if (gesMediaSession?.controller?.playbackState?.state == PlaybackStateCompat.STATE_PAUSED) {
                    gesMediaSession?.isActive = true
                    gesMediaPlayer?.start()
                    setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING)
                    registerNoisyReceiver()

                    val state = gesMediaSession?.controller?.playbackState
                    val token = gesMediaSession!!.sessionToken
                    musicNotificationManager.showNotification(state!!, token)
                }


            }

            override fun onPause() {
                super.onPause()
                if (gesMediaPlayer!!.isPlaying) {
                    gesMediaPlayer!!.pause()
                    unregisterReceiver(becomingNoisyReceiver)

                    setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
                    stopForeground(false)
                    val state = gesMediaSession?.controller?.playbackState
                    val token = gesMediaSession!!.sessionToken
                    musicNotificationManager.showNotification(state!!, token)
                }

            }

            override fun onSkipToNext() {
                super.onSkipToNext()

                if (songQueue.pos == songQueue.queue.size - 1) {
                    songQueue.pos = 0
                } else {
                    songQueue.pos++
                }
                setSourceFromQueue()
            }

            override fun onSkipToPrevious() {
                super.onSkipToPrevious()

                if (songQueue.pos == 0) {
                    songQueue.pos = songQueue.queue!!.size - 1
                } else {
                    songQueue.pos--
                }
                setSourceFromQueue()
            }

            override fun onStop() {
                super.onStop()

                val am = (getSystemService(Context.AUDIO_SERVICE) as AudioManager)
                am.abandonAudioFocus(this@PlaybackService)

                if (gesMediaSession?.controller?.playbackState?.state == PlaybackStateCompat.STATE_PLAYING) {
                    unregisterReceiver(becomingNoisyReceiver)
                }
                if (isServiceStarted) {
                    unregisterReceiver(notificationPlaybackCommand)
                }
                stopSelf()
                isServiceStarted = false
                gesMediaSession?.isActive = false
                gesMediaPlayer?.stop()
                stopForeground(true)
            }


            /* override fun onPlayFromMediaId(mediaId: String?, extras: Bundle) */

            override fun onSeekTo(pos: Long) {
                super.onSeekTo(pos)
            }

            override fun onMediaButtonEvent(mediaButtonEvent: Intent?): Boolean {
                return super.onMediaButtonEvent(mediaButtonEvent)
            }

        }

    private fun setSourceFromQueue() {

        if (songQueue.hasChanged or (songQueue.pos != lastAudioIndex)) {
            val id = songQueue.queue[songQueue.pos].id

            activeAudio = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id
            )
            lastAudioIndex = songQueue.pos

            songQueue.hasChanged = false
        }

        gesMediaPlayer?.reset()
        initMediaPlayer()

        try {
            gesMediaPlayer?.setDataSource(applicationContext, activeAudio!!)
            gesMediaPlayer?.prepareAsync()
        } catch (e: IOException) {
            e.printStackTrace()
            stopSelf()
        }

    }

    //------------------------------AudioFocus, Calls and Broadcasts-------------------------------//

    private var onGoingCall = false
    private var phoneStateListener: PhoneStateListener? = null
    private lateinit var telephonyManager: TelephonyManager


    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_LOSS -> {
                if (gesMediaPlayer!!.isPlaying) {
                    gesMediaPlayer?.pause()
                    setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
                    val state = gesMediaSession?.controller?.playbackState
                    val token = gesMediaSession!!.sessionToken
                    musicNotificationManager.showNotification(state!!, token)
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                if (gesMediaPlayer!!.isPlaying) {
                    gesMediaPlayer?.pause()
                    setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
                    val state = gesMediaSession?.controller?.playbackState
                    val token = gesMediaSession!!.sessionToken
                    musicNotificationManager.showNotification(state!!, token)
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                if (gesMediaPlayer != null) {
                    gesMediaPlayer!!.setVolume(0.2f, 0.2f)
                }
            }
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (gesMediaPlayer != null) {
                    if (gesMediaPlayer!!.isPlaying) {
                        gesMediaPlayer?.start()
                        setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING)
                        val state = gesMediaSession?.controller?.playbackState
                        val token = gesMediaSession!!.sessionToken
                        musicNotificationManager.showNotification(state!!, token)
                    }
                    gesMediaPlayer?.setVolume(1.0f, 1.0f)
                }
            }
        }
    }


    private fun requestAudioFocus(): Boolean {
        val audioManager = (getSystemService(Context.AUDIO_SERVICE) as AudioManager)
        val result = audioManager.requestAudioFocus(
            this,
            AudioManager.STREAM_MUSIC,
            AudioManager.AUDIOFOCUS_GAIN
        )
        return result == AudioManager.AUDIOFOCUS_GAIN
    }

    private fun callStateListener() {
        telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        phoneStateListener = object : PhoneStateListener() {
            override fun onCallStateChanged(state: Int, phoneNumber: String?) {
                when (state) {
                    TelephonyManager.CALL_STATE_OFFHOOK or TelephonyManager.CALL_STATE_RINGING -> {
                        if (gesMediaPlayer != null && gesMediaPlayer!!.isPlaying) {
                            gesMediaPlayer!!.pause()
                            pausedPosition = gesMediaPlayer!!.currentPosition
                            setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
                            onGoingCall = true
                        }
                    }
                    TelephonyManager.CALL_STATE_IDLE -> {
                        if (gesMediaPlayer != null) {
                            if (onGoingCall) {
                                onGoingCall = false
                                gesMediaPlayer!!.seekTo(pausedPosition)
                                gesMediaPlayer!!.start()
                                setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING)
                            }
                        }
                    }
                }
            }
        }

    }

    private val becomingNoisyReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (gesMediaPlayer != null && gesMediaPlayer!!.isPlaying) {
                gesMediaPlayer!!.pause()
                setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
                val state = gesMediaSession?.controller?.playbackState
                val token = gesMediaSession!!.sessionToken
                musicNotificationManager.showNotification(state!!, token)
            }
        }
    }

    private val audioLoadComplete = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            //TODO used to load content into audioList
        }
    }


    private val audioLoadAsyncComplete = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            //TODO used to load content into utils
        }
    }

    private val notificationPlaybackCommand = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == null)
                return

            val actionString = intent.action
            when {
                actionString.equals(ACTION_PLAY, true) -> {
                    mTransportControls?.play()
                }
                actionString.equals(ACTION_PAUSE, true) -> {
                    mTransportControls?.pause()
                }
                actionString.equals(ACTION_NEXT, true) -> {
                    mTransportControls?.skipToNext()
                }
                actionString.equals(ACTION_PREVIOUS, true) -> {
                    mTransportControls?.skipToPrevious()
                }
                actionString.equals(ACTION_STOP, true) -> {
                    mTransportControls?.stop()
                }
            }
        }
    }

    private fun refreshQueueAndStart() {
        setSourceFromQueue()
        mTransportControls?.play()
    }


    //----------------------------------------Static----------------------------------------//

    companion object {
        var isServiceStarted = false

        private var lastAudioIndex = -1

        const val ACTION_PLAY = "com.teczer.musicplayer.ACTION_PLAY"
        const val ACTION_PAUSE = "com.teczer.musicplayer.ACTION_PAUSE"
        const val ACTION_PREVIOUS = "com.teczer.musicplayer.ACTION_PREVIOUS"
        const val ACTION_NEXT = "com.teczer.musicplayer.ACTION_NEXT"
        const val ACTION_STOP = "com.teczer.musicplayer.ACTION_STOP"

        const val ACTION_START_QUEUE = "com.teczer.musicplayer.ACTION_START_QUEUE"


    }

    /*-----------------------------------------------------------------------------------------*/

    override fun onLoadChildren(
        parentId: String,
        result: Result<MutableList<MediaBrowserCompat.MediaItem>>
    ) {
        result.sendResult(null)
    }

    override fun onGetRoot(
        clientPackageName: String,
        clientUid: Int,
        rootHints: Bundle?
    ): BrowserRoot? {
        return BrowserRoot(getString(R.string.app_name), null)
    }


}