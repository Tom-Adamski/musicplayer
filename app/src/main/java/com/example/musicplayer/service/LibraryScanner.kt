package com.example.musicplayer.service

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.MediaStore
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.room.Room
import com.example.musicplayer.R
import com.example.musicplayer.database.AppDatabase
import com.example.musicplayer.database.dao.AlbumDao
import com.example.musicplayer.database.dao.ArtistDao
import com.example.musicplayer.database.dao.FeaturingDao
import com.example.musicplayer.database.dao.SongDao
import com.example.musicplayer.database.entity.Song
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey
import java.io.File


class LibraryScanner : IntentService("LibraryScanner") {

    private lateinit var db : AppDatabase
    private lateinit var artistDao: ArtistDao
    private lateinit var albumDao: AlbumDao
    private lateinit var songDao: SongDao
    private lateinit var featuringDao: FeaturingDao

    override fun onCreate() {
        super.onCreate()
        db = Room.databaseBuilder(this, AppDatabase::class.java, "music-database").build()
        artistDao = db.artistDao()
        albumDao = db.albumDao()
        songDao = db.songDao()
        featuringDao = db.featuringDao()
    }

    override fun onHandleIntent(intent: Intent?) {
        when (intent?.action) {
            ACTION_SCAN -> {
                scan()
            }
            ACTION_RESET -> {
                reset()
            }
        }
    }

    private fun scan(){

        createNotificationChannel()

        var builder = NotificationCompat.Builder(this, getString(R.string.notification_scan_channel_id))
            .setSmallIcon(R.drawable.ic_sync_black_24dp)
            .setContentTitle(getString(R.string.notification_title_scan))
            .setContentText(getString(R.string.notification_text_scan))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(0, builder.build())
        }

        getMedia()

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            cancel(0)
        }

    }

    private fun getMedia() {

        val selection = MediaStore.Audio.Media.IS_MUSIC + " != 0"

        val projection = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.DATA
        )

        val cursor = contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            projection,
            selection,
            null, null
        )


        while (cursor != null && cursor.moveToNext()) {


            var songId : Long = cursor.getString(0).toLong()
            var duration = cursor.getString(1).toInt()
            var path = cursor.getString(2)

            val file = File(path)
            val audioFile = AudioFileIO.read(file)
            val tag = audioFile.tag

            val title = tag.getFirst(FieldKey.TITLE)
            val artistName = tag.getFirst(FieldKey.ARTIST)
            val albumName = tag.getFirst(FieldKey.ALBUM)
            val albumArtist = tag.getFirst(FieldKey.ALBUM_ARTIST)
            val track = tag.getFirst(FieldKey.TRACK).toInt()
            val date = tag.getFirst(FieldKey.YEAR)

            val artists = artistName.split(Regex("( ?; ?)|( ?[/] ?)"))


            val artistId = artistDao.upsert(artists[0], null)

            val albumArtistId = artistDao.upsert(albumArtist, null)
            val albumId = albumDao.upsert(albumName, albumArtistId, null, date)

            songDao.insert(Song(songId, title, albumId, artistId, track, duration))

            for(i in 1 until artists.size){
                val featId = artistDao.upsert(artists[i], null)
                featuringDao.upsert(songId, featId)
            }

        }

        cursor?.close()

    }

    private fun reset(){
        db.resetDatabase()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notification_scan_channel_name)
            val descriptionText = getString(R.string.notification_scan_channel_desc)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(getString(R.string.notification_scan_channel_id), name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        const val ACTION_SCAN = "com.example.musicplayer.service.action.SCAN"
        const val ACTION_RESET = "com.example.musicplayer.service.action.RESET"
    }
}
