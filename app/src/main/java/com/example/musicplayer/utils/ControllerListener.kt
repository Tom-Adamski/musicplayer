package com.example.musicplayer.utils

interface ControllerListener {
    fun onControllerClick()
}