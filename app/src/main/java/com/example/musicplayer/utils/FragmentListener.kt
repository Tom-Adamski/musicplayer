package com.example.musicplayer.utils

public interface FragmentListener {

    fun onArtistClick(artistId : Long)

    fun onAlbumClick(albumId : Long)

}