package com.example.musicplayer.utils

import android.content.Context
import com.example.musicplayer.container.NestedSong
import com.example.musicplayer.database.entity.SongDetails

class SongQueue {

    val queue = ArrayList<SongDetails>()
    var pos = -1

    var hasChanged = false


    fun setQueue(list : ArrayList<SongDetails>){
        queue.clear()
        queue.addAll(list)
    }

    /* get : previous current next */

    fun gp() : SongDetails{
        if(pos == 0){
            return queue[queue.size-1]
        }
        return queue[pos-1]
    }

    fun cu() : SongDetails{
        return queue[pos]
    }

    fun ne() : SongDetails{
        if(pos == queue.size-1){
            return queue[0]
        }
        return queue[pos+1]
    }




    companion object {
        private var songQueue : SongQueue? = null

        fun getInstance(context : Context) : SongQueue{
            if(songQueue == null){
                songQueue = SongQueue()
            }

            return songQueue!!
        }
    }
}