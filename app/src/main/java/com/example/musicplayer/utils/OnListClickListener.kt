package com.example.musicplayer.utils

public interface OnListClickListener {

    fun onListClick(position: Int)

}