package com.example.musicplayer.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Build
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.activity.MainActivity
import com.example.musicplayer.service.PlaybackService


class MusicNotificationManager(
    val context: Context
) {

    val playPauseSongIntent = Intent()
    val previousSongIntent = Intent(PlaybackService.ACTION_PREVIOUS)
    val nextSongIntent = Intent(PlaybackService.ACTION_NEXT)

    val largeIcon = BitmapFactory.decodeResource(context.resources, R.drawable.large)

    var playPauseNotificationIcon = -1

    val notificationClickIntent = Intent(context, MainActivity::class.java)
        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

    val clickActivityStart = PendingIntent.getActivity(context, 0, notificationClickIntent, 0)

    val mNotificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    val songQueue : SongQueue

    val previousSongPendingIntent: PendingIntent
    val nextSongPendingIntent: PendingIntent
    lateinit var playPauseSongPendingIntent: PendingIntent

    init {
        previousSongPendingIntent = PendingIntent.getBroadcast(context, 0, previousSongIntent, 0)
        nextSongPendingIntent = PendingIntent.getBroadcast(context, 0, nextSongIntent, 0)



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                CHANNEL_IMPORTANCE
            )
            channel.description = CHANNEL_DESCRIPTION
            mNotificationManager.createNotificationChannel(channel)
        }

        songQueue = SongQueue.getInstance(context)

    }

    fun setPlayPausePendingIntent(state: PlaybackStateCompat) {
        when (state.state) {
            PlaybackStateCompat.STATE_PLAYING -> {
                playPauseNotificationIcon = R.drawable.ic_pause_black_24dp
                playPauseSongIntent.action = PlaybackService.ACTION_PAUSE
                playPauseSongPendingIntent = PendingIntent.getBroadcast(
                    context, 0,
                    playPauseSongIntent, 0
                )
            }
            else -> {
                playPauseNotificationIcon = R.drawable.ic_play_arrow_black_24dp
                playPauseSongIntent.action = PlaybackService.ACTION_PLAY
                playPauseSongPendingIntent = PendingIntent.getBroadcast(
                    context, 0,
                    playPauseSongIntent, 0
                )
            }
        }
    }


    fun showNotification(
        state: PlaybackStateCompat,
        mediaSessionToken: MediaSessionCompat.Token,
        service: PlaybackService? = null
    ) {
        setPlayPausePendingIntent(state)

        val title = songQueue.queue[songQueue.pos].title
        val artist = songQueue.queue[songQueue.pos].artistName
        val thumbnailUrl = songQueue.queue[songQueue.pos].thumbnail

        val notificationBuilder = NotificationCompat.Builder(context, CHANNEL_ID)

        notificationBuilder.apply {
            setContentIntent(clickActivityStart)
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

            setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mediaSessionToken)
                    .setShowActionsInCompactView(0, 1, 2)
            )

            setSmallIcon(R.drawable.ic_album_black_24dp)

            setContentTitle(title)
            setContentText(artist)
            setSubText("Album")
            setShowWhen(false)

            addAction(R.drawable.ic_skip_previous_black_24dp, "Previous", previousSongPendingIntent)
            addAction(playPauseNotificationIcon, "PlayPause", playPauseSongPendingIntent)
            addAction(R.drawable.ic_skip_next_black_24dp, "Next", nextSongPendingIntent)
        }

        AsyncTask.execute {
            try {
                val futureTarget = Glide.with(context)
                    .asBitmap()
                    .load(thumbnailUrl)
                    .submit()

                val thumbnail = futureTarget.get()

                notificationBuilder.setLargeIcon(thumbnail)
            } catch (e: Exception) {
                notificationBuilder.setLargeIcon(largeIcon)
            }

            val notification = notificationBuilder.build()
            if (service == null) {
                mNotificationManager.notify(NOTIFICATION_ID, notification)
            } else {
                service.startForeground(NOTIFICATION_ID, notification)
            }
        }
    }


    companion object {

        private var musicNotificationManager: MusicNotificationManager? = null

        fun getInstance(context: Context): MusicNotificationManager {
            if (musicNotificationManager == null) {
                musicNotificationManager = MusicNotificationManager(context)
            }
            return musicNotificationManager!!
        }


        const val NOTIFICATION_ID = 123
        const val CHANNEL_ID = "com.teczer.musicplayer.NOTIFICATION_CHANNEL_ID"
        const val CHANNEL_NAME = "Music Notification Channel"
        const val CHANNEL_DESCRIPTION = "This is the Music Player notification channel"
        const val CHANNEL_IMPORTANCE = NotificationManager.IMPORTANCE_DEFAULT


        const val ACTION_PLAY = "com.teczer.musicplayer.ACTION_PLAY"
        const val ACTION_PAUSE = "com.teczer.musicplayer.ACTION_PAUSE"
        const val ACTION_PREVIOUS = "com.teczer.musicplayer.ACTION_PREVIOUS"
        const val ACTION_NEXT = "com.teczer.musicplayer.ACTION_NEXT"
        const val ACTION_STOP = "com.teczer.musicplayer.ACTION_STOP"

        const val ACTION_START_QUEUE = "com.teczer.musicplayer.ACTION_START_QUEUE"
    }


}