package com.example.musicplayer.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.room.Room
import com.example.musicplayer.R
import com.example.musicplayer.database.AppDatabase
import com.example.musicplayer.database.dao.AlbumDao
import com.example.musicplayer.database.dao.ArtistDao
import com.example.musicplayer.network.GeniusApi
import com.example.musicplayer.network.json.album.Album
import com.example.musicplayer.network.json.artist.Artist
import com.example.musicplayer.service.LibraryScanner
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SettingsActivity : AppCompatActivity() {

    private lateinit var db: AppDatabase
    private lateinit var artistDao: ArtistDao
    private lateinit var albumDao: AlbumDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(findViewById(R.id.toolbar))

        db = Room.databaseBuilder(this, AppDatabase::class.java, "music-database").build()
        artistDao = db.artistDao()
        albumDao = db.albumDao()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_quit -> {
            finish()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    fun scan(view: View) {
        if (Build.VERSION.SDK_INT >= 23 &&
            (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                0
            )
        } else {
            startScanService()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startScanService()
        }
    }

    fun startScanService() {
        val intent = Intent(this, LibraryScanner::class.java)
        intent.action = LibraryScanner.ACTION_SCAN
        startService(intent)
    }

    fun reset(view: View) {
        val intent = Intent(this, LibraryScanner::class.java)
        intent.action = LibraryScanner.ACTION_RESET
        startService(intent)
    }

    fun fetchArtists(view: View) {

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://genius.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val geniusApi = retrofit.create(GeniusApi::class.java)

        AsyncTask.execute {
            artistDao.getAllWithoutThumbnail().forEach {

                val call = geniusApi.artists(it.name)

                call.enqueue(object : Callback<Artist> {
                    override fun onResponse(
                        call: Call<Artist>, response: Response<Artist>
                    ) {
                        try {
                            val url =
                                response.body()!!.response.sections[0].hits[0].result.image_url

                            AsyncTask.execute {
                                artistDao.updateThumbnail(it.id, url)
                            }
                        } catch (e: Exception) {/*No artist thumbnail*/
                        }
                    }

                    override fun onFailure(call: Call<Artist>, t: Throwable) {}
                })
            }
        }
    }


    fun fetchAlbums(view: View) {

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://genius.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val geniusApi = retrofit.create(GeniusApi::class.java)

        AsyncTask.execute {
            albumDao.getAllWithoutThumbnail().forEach { album ->

                val artist = artistDao.loadById(album.artistId)

                val call = geniusApi.albums("${album.title} ${artist.name} ")

                call.enqueue(object : Callback<Album> {
                    override fun onResponse(
                        call: Call<Album>, response: Response<Album>
                    ) {
                        try {
                            val url =
                                response.body()!!.response.sections[0].hits[0].result.cover_art_thumbnail_url

                            AsyncTask.execute {
                                albumDao.updateThumbnail(album.id, url)
                            }
                        } catch (e: Exception) {/*No artist thumbnail*/
                        }
                    }

                    override fun onFailure(call: Call<Album>, t: Throwable) {}
                })
            }
        }

    }


}
