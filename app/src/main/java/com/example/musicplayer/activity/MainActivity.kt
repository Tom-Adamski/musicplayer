package com.example.musicplayer.activity

import android.content.ComponentName
import android.content.Intent
import android.media.session.PlaybackState.STATE_PAUSED
import android.media.session.PlaybackState.STATE_PLAYING
import android.os.Bundle
import android.os.RemoteException
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import androidx.viewpager2.widget.ViewPager2
import com.example.musicplayer.R
import com.example.musicplayer.adapter.ControllerAdapter
import com.example.musicplayer.fragment.AlbumFragment
import com.example.musicplayer.fragment.ArtistFragment
import com.example.musicplayer.fragment.LibraryFragment
import com.example.musicplayer.service.PlaybackService
import com.example.musicplayer.utils.ControllerListener
import com.example.musicplayer.utils.FragmentListener
import com.example.musicplayer.utils.SongQueue
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), FragmentListener, ControllerListener {

    /* ----------------------- UI Components -------------------------------*/

    private val libraryFragment = LibraryFragment()
    private val constraintSetLibraryWithController = ConstraintSet()
    private val constraintSetLibraryWithoutController = ConstraintSet()
    private val constraintSetNowPlayingWithController = ConstraintSet()
    private val constraintSetNowPlayingWithoutController = ConstraintSet()
    private var isNowPlayingOpen = false
    private var isControllerOpen = false
    private val transition = AutoTransition().setDuration(100)


    /* ----------------------- Queue -------------------------------*/

    private lateinit var songQueue: SongQueue


    /* ----------------------- Media ---------------------------------------- */
    private var mCurrentState = STATE_PAUSED

    private lateinit var mMediaBrowser: MediaBrowserCompat
    private lateinit var mMediaController: MediaControllerCompat

    private val mMediaBrowserConnectionCallback: MediaBrowserCompat.ConnectionCallback =
        object : MediaBrowserCompat.ConnectionCallback() {
            override fun onConnected() {
                super.onConnected()
                try {
                    mMediaController =
                        MediaControllerCompat(this@MainActivity, mMediaBrowser.sessionToken)
                    mMediaController.registerCallback(mMediaControllerCallback)

                } catch (e: RemoteException) {
                }
            }
        }

    private val mMediaControllerCallback: MediaControllerCompat.Callback =
        object : MediaControllerCompat.Callback() {
            override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
                super.onPlaybackStateChanged(state)
                if (state == null) {
                    return
                }
                when (state.state) {
                    PlaybackStateCompat.STATE_PLAYING -> {
                        mCurrentState = STATE_PLAYING
                        play_pause_button.setImageResource(R.drawable.ic_pause_black_24dp)
                    }
                    PlaybackStateCompat.STATE_PAUSED -> {
                        mCurrentState = STATE_PAUSED
                        play_pause_button.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                    }
                }
            }

            override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
                super.onMetadataChanged(metadata)
                updateController()
            }
        }


    /* ----------------------- Activity Lifecycle ---------------------------------------- */


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        songQueue = SongQueue.getInstance(this)

        // Create media background service
        mMediaBrowser = MediaBrowserCompat(
            this, ComponentName(this, PlaybackService::class.java),
            mMediaBrowserConnectionCallback, intent.extras
        )
        mMediaBrowser.connect()

        // Default fragment
        setFragment(FRAGTYPE.LIBRARY)

        // Controller
        hideController()
        controller.adapter = ControllerAdapter(this)
        controller.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 0) {
                    previous()
                } else if (position == 2) {
                    next()
                }
            }
        })

        // Constraints for layout changes
        constraintSetLibraryWithController.apply {
            clone(activity_main)
            setVisibility(R.id.separation, View.VISIBLE)
            setVisibility(R.id.controller, View.VISIBLE)
            setVisibility(R.id.play_pause_button, View.VISIBLE)
        }
        constraintSetLibraryWithoutController.apply {
            clone(activity_main)
            setVisibility(R.id.separation, View.GONE)
            setVisibility(R.id.controller, View.GONE)
            setVisibility(R.id.play_pause_button, View.GONE)
        }
        constraintSetNowPlayingWithController.apply {
            clone(activity_main)
            setVisibility(R.id.fragment_placeholder, View.GONE)
            setVisibility(R.id.now_playing_placeholder, View.VISIBLE)
            setVisibility(R.id.separation, View.VISIBLE)
            setVisibility(R.id.controller, View.VISIBLE)
            setVisibility(R.id.play_pause_button, View.VISIBLE)
        }
        constraintSetNowPlayingWithoutController.apply {
            clone(activity_main)
            setVisibility(R.id.fragment_placeholder, View.GONE)
            setVisibility(R.id.now_playing_placeholder, View.VISIBLE)
            setVisibility(R.id.separation, View.GONE)
            setVisibility(R.id.controller, View.GONE)
            setVisibility(R.id.play_pause_button, View.GONE)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (mMediaController.playbackState?.state == STATE_PLAYING) {
            mMediaController.transportControls.stop()
        }

        mMediaBrowser.disconnect()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    private fun setFragment(which: FRAGTYPE, id: Long = -1) {


        supportFragmentManager.beginTransaction().apply {
            when (which) {
                FRAGTYPE.LIBRARY -> {
                    add(R.id.fragment_placeholder, libraryFragment, which.tag)
                    commit()
                }
                FRAGTYPE.ARTIST -> {
                    add(R.id.fragment_placeholder, ArtistFragment(id), which.tag)
                    hide(libraryFragment)
                    addToBackStack(null)
                    commit()
                }
                FRAGTYPE.ALBUM -> {
                    add(R.id.fragment_placeholder, AlbumFragment(id), which.tag)
                    hide(libraryFragment)
                    addToBackStack(null)
                    commit()
                }
            }
        }


    }

    private fun showController() {
        separation.visibility = View.VISIBLE
        controller.visibility = View.VISIBLE
        play_pause_button.visibility = View.VISIBLE
        isControllerOpen = true
    }

    private fun hideController() {
        separation.visibility = View.GONE
        controller.visibility = View.GONE
        play_pause_button.visibility = View.GONE
        isControllerOpen = false
    }

    fun updateController() {
        (controller.adapter as ControllerAdapter).updateList(songQueue)
        controller.setCurrentItem(1, false)
        showController()
    }

    private fun switchNowPlaying(){
        if(isNowPlayingOpen){
            if(isControllerOpen){
                TransitionManager.beginDelayedTransition(activity_main, transition)
                constraintSetLibraryWithController.applyTo(activity_main)
            }
            else {
                TransitionManager.beginDelayedTransition(activity_main, transition)
                constraintSetLibraryWithoutController.applyTo(activity_main)
            }
            isNowPlayingOpen = false
        }
        else {
            if(isControllerOpen){
                TransitionManager.beginDelayedTransition(activity_main, transition)
                constraintSetNowPlayingWithController.applyTo(activity_main)
            }
            else {
                TransitionManager.beginDelayedTransition(activity_main, transition)
                constraintSetNowPlayingWithoutController.applyTo(activity_main)
            }
            isNowPlayingOpen = true
        }


    }


    //--------------------------- User Interaction ---------------------------------------//

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val settingsIntent = Intent(this, SettingsActivity::class.java)
            startActivity(settingsIntent)
            true
        }
        R.id.action_search -> {
            switchNowPlaying()
            true
        }
        R.id.action_favorite -> {
            switchNowPlaying()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    fun playPause(@Suppress("UNUSED_PARAMETER")view: View) {
        mCurrentState = when (mCurrentState) {
            STATE_PAUSED -> {
                mMediaController.transportControls.play()
                STATE_PLAYING
            }
            else -> {
                if (mMediaController.playbackState!!.state == STATE_PLAYING) {
                    mMediaController.transportControls.pause()
                }
                STATE_PAUSED
            }
        }
    }

    fun previous() {
        mMediaController.transportControls.skipToPrevious()
    }

    fun next() {
        mMediaController.transportControls.skipToNext()
    }

    override fun onArtistClick(artistId: Long) {
        setFragment(FRAGTYPE.ARTIST, artistId)
    }

    override fun onAlbumClick(albumId: Long) {
        setFragment(FRAGTYPE.ALBUM, albumId)
    }

    override fun onControllerClick() {
        switchNowPlaying()
    }


    override fun onBackPressed() {
        when {
            isNowPlayingOpen -> {
                switchNowPlaying()
            }
            supportFragmentManager.backStackEntryCount > 0 -> {
                super.onBackPressed()
            }
            else -> {
                this.moveTaskToBack(true)
            }
        }
    }



    companion object {

        enum class FRAGTYPE(val tag: String) {
            LIBRARY("LIBRARY"),
            ARTIST("ARTIST"),
            ALBUM("ALBUM"),
        }


    }


}
