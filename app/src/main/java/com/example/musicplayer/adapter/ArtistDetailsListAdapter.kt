package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.database.entity.ArtistDetails
import com.example.musicplayer.utils.OnListClickListener
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import kotlinx.android.synthetic.main.item_artist.view.*


class ArtistDetailsListAdapter(
    private val artistList: ArrayList<ArtistDetails>,
    private val itemClickListener: OnListClickListener
) : RecyclerView.Adapter<ArtistDetailsListAdapter.ArtistDetailsViewHolder>(), SectionTitleProvider {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistDetailsViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_artist, parent, false)

        return ArtistDetailsViewHolder(item)
    }

    override fun onBindViewHolder(holder: ArtistDetailsViewHolder, position: Int) {
        holder.bind(artistList[position], itemClickListener, position)

    }


    override fun getItemCount(): Int {
        return artistList.size
    }

    override fun getSectionTitle(position: Int): String {
        return artistList[position].name.substring(0, 1)
    }


    class ArtistDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val artistImage = itemView.artist_art
        val artistName = itemView.artist_name
        val artistStats = itemView.artist_stats

        fun bind(artist: ArtistDetails, clickListener: OnListClickListener, position: Int) {
            artistName.text = artist.name

            var stats = ""
            if(artist.own > 0){
                stats += "${artist.own} song"
                if(artist.own > 1){
                    stats += "s"
                }
                stats += " "
            }
            if(artist.feats > 0){
                stats += "${artist.feats} featuring"
                if(artist.feats > 1){
                    stats += "s"
                }
                stats += " "
            }
            if(artist.albums > 0){
                stats += "${artist.albums} album"
                if(artist.albums > 1){
                    stats += "s"
                }
            }

            artistStats.text = stats

            itemView.setOnClickListener {
                clickListener.onListClick(position)
            }

            Glide.with(itemView)
                .load(artist.thumbnail)
                .circleCrop()
                .error(R.drawable.ic_person_black_24dp)
                .into(artistImage)

        }

    }

}