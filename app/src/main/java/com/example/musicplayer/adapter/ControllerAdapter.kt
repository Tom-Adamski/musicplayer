package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.container.NestedSong
import com.example.musicplayer.database.entity.SongDetails
import com.example.musicplayer.utils.ControllerListener
import com.example.musicplayer.utils.SongQueue
import kotlinx.android.synthetic.main.item_controller.view.*

class ControllerAdapter(private val listener: ControllerListener) : RecyclerView.Adapter<ControllerAdapter.ControllerViewHolder>() {

    private val songList = ArrayList<SongDetails>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControllerViewHolder {

        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_controller, parent, false)

        item.setOnClickListener {
            listener.onControllerClick()
        }

        return ControllerViewHolder(item)
    }

    override fun onBindViewHolder(holder: ControllerViewHolder, position: Int) {
        holder.bind(songList[position])
    }


    override fun getItemCount(): Int {
        return songList.size
    }

    fun updateList(songQueue: SongQueue) {
        songList.clear()
        songList.addAll(arrayListOf(songQueue.gp(), songQueue.cu(), songQueue.ne()))
        notifyDataSetChanged()
    }


    class ControllerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val songTitle = itemView.song_title
        private val songArtist = itemView.album_artist
        private val songCover = itemView.song_cover

        fun bind(song: SongDetails) {
            songTitle.text = song.title
            songArtist.text = song.artistName

            songTitle.isSelected = true
            songArtist.isSelected = true

            Glide.with(itemView)
                .load(song.thumbnail)
                .error(R.drawable.ic_album_black_24dp)
                .into(songCover)

        }
    }
}