package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.database.entity.AlbumDetails
import com.example.musicplayer.utils.OnListClickListener
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import kotlinx.android.synthetic.main.item_album.view.*

class AlbumDetailsListAdapter(
    private val albumList: ArrayList<AlbumDetails>,
    private val itemClickListener: OnListClickListener
) : RecyclerView.Adapter<AlbumDetailsListAdapter.AlbumDetailsViewHolder>(), SectionTitleProvider {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumDetailsViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_album, parent, false)

        return AlbumDetailsViewHolder(item)
    }

    override fun onBindViewHolder(holder: AlbumDetailsViewHolder, position: Int) {
        holder.bind(albumList[position], itemClickListener, position)

    }


    override fun getItemCount(): Int {
        return albumList.size
    }

    override fun getSectionTitle(position: Int): String {
        return albumList[position].title.substring(0, 1)
    }


    class AlbumDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val albumImage = itemView.album_cover
        val albumTitle = itemView.album_title
        val albumDate = itemView.date
        val albumArtist = itemView.album_artist
        val albumTracks = itemView.tracks

        fun bind(album: AlbumDetails, clickListener: OnListClickListener, position: Int) {
            albumTitle.text = album.title
            albumDate.text = album.date
            albumArtist.text = album.artistName
            var tracks = "${album.tracks} song"
            if(album.tracks > 1){
                tracks += "s"
            }
            albumTracks.text = tracks


            itemView.setOnClickListener {
                clickListener.onListClick(position)
            }

            Glide.with(itemView)
                .load(album.thumbnail)
                .error(R.drawable.ic_album_black_24dp)
                .into(albumImage)

        }

    }

}