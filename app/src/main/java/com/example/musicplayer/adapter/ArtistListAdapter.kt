package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.database.entity.Artist
import com.example.musicplayer.utils.OnListClickListener
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import kotlinx.android.synthetic.main.item_artist.view.*


class ArtistListAdapter(
    private val artistList: ArrayList<Artist>,
    private val itemClickListener: OnListClickListener
) : RecyclerView.Adapter<ArtistListAdapter.ArtistViewHolder>(), SectionTitleProvider {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_artist, parent, false)

        return ArtistViewHolder(item)
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        holder.bind(artistList[position], itemClickListener, position)

    }


    override fun getItemCount(): Int {
        return artistList.size
    }

    override fun getSectionTitle(position: Int): String {
        return artistList[position].name.substring(0, 1)
    }


    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val artistImage = itemView.artist_art
        val artistName = itemView.artist_name

        fun bind(artist: Artist, clickListener: OnListClickListener, position: Int) {
            artistName.text = artist.name
            itemView.setOnClickListener {
                clickListener.onListClick(position)
            }

            Glide.with(itemView)
                .load(artist.thumbnail)
                .circleCrop()
                .error(R.drawable.ic_person_black_24dp)
                .into(artistImage)

        }

    }

}