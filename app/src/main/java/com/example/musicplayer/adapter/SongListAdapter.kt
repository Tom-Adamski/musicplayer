package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.container.NestedSong
import com.example.musicplayer.utils.OnListClickListener
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import kotlinx.android.synthetic.main.item_song.view.*

class SongListAdapter(
    private val songList: ArrayList<NestedSong>,
    private val itemClickListener: OnListClickListener
) :
    RecyclerView.Adapter<SongListAdapter.SongViewHolder>(), SectionTitleProvider {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_song, parent, false)

        return SongViewHolder(item)
    }


    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        holder.bind(songList[position], itemClickListener, position)

    }

    override fun getItemCount(): Int {
        return songList.size
    }

    override fun getSectionTitle(position: Int): String {
        return songList[position].title.substring(0, 1)
    }

    class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val songTitle = itemView.song_title
        val songArtist = itemView.album_artist
        val songCover = itemView.song_cover

        fun bind(song: NestedSong, clickListener: OnListClickListener, position: Int) {
            songTitle.text = song.title
            songArtist.text = song.artistName
            itemView.setOnClickListener {
                clickListener.onListClick(position)
            }

            Glide.with(itemView)
                .load(song.thumbnail)
                .error(R.drawable.ic_album_black_24dp)
                .into(songCover)
        }

    }


}