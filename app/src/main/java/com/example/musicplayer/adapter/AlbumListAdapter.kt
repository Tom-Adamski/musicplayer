package com.example.musicplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicplayer.R
import com.example.musicplayer.database.entity.Album
import com.example.musicplayer.utils.OnListClickListener
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
import kotlinx.android.synthetic.main.item_album.view.*

class AlbumListAdapter(
    private val albumList: ArrayList<Album>,
    private val itemClickListener: OnListClickListener
) : RecyclerView.Adapter<AlbumListAdapter.AlbumViewHolder>(), SectionTitleProvider {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_album, parent, false)

        return AlbumViewHolder(item)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(albumList[position], itemClickListener, position)

    }


    override fun getItemCount(): Int {
        return albumList.size
    }

    override fun getSectionTitle(position: Int): String {
        return albumList[position].title.substring(0, 1)
    }


    class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val albumImage = itemView.album_cover
        val albumTitle = itemView.album_title

        fun bind(album: Album, clickListener: OnListClickListener, position: Int) {
            albumTitle.text = album.title
            itemView.setOnClickListener {
                clickListener.onListClick(position)
            }

            Glide.with(itemView)
                .load(album.thumbnail)
                .error(R.drawable.ic_album_black_24dp)
                .into(albumImage)

        }

    }

}