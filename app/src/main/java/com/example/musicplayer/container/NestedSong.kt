package com.example.musicplayer.container

data class NestedSong(val id : Long, val title: String, val artistName: String, val albumName: String, val thumbnail: String?)